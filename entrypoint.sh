#!/bin/sh

set -ex

if [ "$POSTGRES_DATABASE" = "test-app" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $POSTGRES_HOST $POSTGRES_PORT; do
      sleep 0.1
    done

    echo "Postgres started"
fi

chmod -R 666 ./var/log

python /app/src/manage.py makemigrations --noinput
python /app/src/manage.py migrate
python /app/src/manage.py create_default_admin
python /app/src/manage.py collectstatic --noinput --clear

chmod -R 777 media
chmod -R 777 static
exec "$@"

python /app/src/manage.py runserver 0.0.0.0:8000