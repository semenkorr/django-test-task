FROM python:3.9-slim-buster
LABEL authors="semenkorikov"
WORKDIR app
RUN apt update && apt upgrade -y
RUN apt -y install python3-dev netcat-traditional
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
COPY . .
RUN pip install -r requirements.txt
EXPOSE 8000
RUN chmod +x entrypoint.sh
RUN mkdir "static"
RUN mkdir "media"
RUN mkdir -p  "var/log"
ENTRYPOINT ["./entrypoint.sh"]