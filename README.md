# Django-test-task



## Цель проекта

[Выполнение тестового задания](https://docs.google.com/document/d/1W_9il7Z3TTp7zJ3KDnwFJnN-FdX9TBXDlslEs12keNc/mobilebasic)


## Задачи

- Реализовать модели департамента и сотрудника
- Реализовать REST-эндпоинты для:
    * Получение списка депортаментов
    * Получение списка сотрудников(с возможностью фильтрации по id депортамента и фамилии сотрудника)
    * Добавление/удаление сотрудников
- Пагинация для списка сотрудников, и ее отсутствие для списка депортаметов
- Разрешения на просмотр списка депортаметов для всех и списка сотрудников только для авторизованных
- Дополнительные вычесляемые поля для депортамента
- Необходимо настроить админку для работы с вышеуказанными моделями
- Необходимо настроить автодокументацию в Swagger

## Требования к выполнению

- Задача должна быть выполнена на фрейворке Django, с использованием библиотеки DRF
- Для маршрутизации нужно использовать класс роутера из DRF
- Контроллеры должны быть реализованы с использованием ModelViewSet

## Использованные технологии и библиотеки

- Django 
- django-filter
- django-rest-framework
- drf-yasg 
- docker
- docker-compose
- nginx
- PostgreSQL

## Запуск проекта

1. Если проект будет запускаться в docker'е:
```sh
git clone git@gitlab.com:semenkorr/django-test-task.git
mv .env.example .env
cd django-test-task
docker-compose up -d
 ```

2. Если проект запускается вручную:
```sh
git clone git@gitlab.com:semenkorr/django-test-task.git
mv .env.example .env
cd django-test-task
mkdir -p var/log
mkdir static media
python -m venv && source ./venv/bin/activate
pip install -r requirements.txt
./src/manage.py makemigrations
./src/manage.py migrate
./src/manage.py runserver
```

## Замечания по проекту

- Хоть и изначально не стояло цели поднимать приложение внутри контейнера,
  для удобства тестирования я решил это реализовать.
- Также для удобства я реализовал management-комманду для созданию дефолтного админа,
  со следующими кредами: username: Admin, email: admin@mail.com, password: admin123,
  в контейнере данная команда запуcкается при сборке.
 
