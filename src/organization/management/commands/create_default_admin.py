from django.core.management.base import BaseCommand
from django.db import DatabaseError
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):
    help = "Создание дефолтного админа платформы"

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Creating default admin'))
        try:
            if not User.objects.filter(email='admin@mail.com', is_superuser=True).exists():
                User.objects.create_superuser('Admin', email='admin@mail.com', password='admin123')
                self.stdout.write(self.style.SUCCESS('Admin create successful'))
            self.stdout.write(self.style.SUCCESS("Admin already exist"))
        except DatabaseError as e:
            self.stdout.write(self.style.ERROR(f'Error in creating default admin, error: {str(e)}'))
