from django.contrib import admin
from organization.models import Department, Employee


@admin.register(Department)
class DepartamentAdmin(admin.ModelAdmin):
    list_display = ('name', 'chief_fio')
    search_filter = ('name', 'chief__name', 'chief__last_name')
    search_help_text = "Введите название департамента или имя/фамилию директора"

    @admin.display(description="ФИО Руководителя")
    def chief_fio(self, obj: Department):
        return obj.chief.fio


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'last_name',
        'patronymic',
        'post',
        'salary',
        'age',
        'departament_name'
    )
    list_filter = ('salary', 'post')
    search_fields = ('name', 'last_name', 'patronymic', 'age')
    search_help_text = 'Введите имя, фамилию, отчество или возраст'

    @admin.display(description='Название депортамента')
    def departament_name(self, obj: Employee):
        return obj.department.name
