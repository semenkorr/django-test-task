import logging
from _decimal import Decimal
from typing import Mapping

from django.db import models
from django.db.models import Sum
from django.db import DatabaseError

logger = logging.getLogger('debug')


class Department(models.Model):
    """ Represent department in organization """
    name = models.CharField("Название", max_length=250)
    chief = models.OneToOneField(
        'Employee',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='employee_department'
    )

    @property
    def employees_count(self) -> int:
        try:
            eml_count: int = self.employees.prefetch_related('employees').count()
            return eml_count
        except DatabaseError as e:
            logger.exception(f"Error in getting employees count: {str(e)}")
            raise e

    @property
    def total_employees_salary(self) -> Decimal:
        try:
            total: Mapping = self.employees.prefetch_related('employees').aggregate(total_salary=Sum('salary'))
            res: Decimal = total['total_salary']
            return res
        except DatabaseError as e:
            logger.exception(f"Error in getting total employees salary: {str(e)}")
            raise e

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'departments'
        verbose_name = 'Департамент'
        verbose_name_plural = 'Департаменты'


class Employee(models.Model):
    """ Represent employee in organization """

    def generate_file_path(self, *args, **kwargs):
        return f"{self.fio}/photo/{self.photo.name}"

    name = models.CharField("Имя сотрудника", max_length=150)
    last_name = models.CharField("Фамилия сотрудника", max_length=150)
    patronymic = models.CharField("Отчество сотрудника", max_length=150)
    post = models.CharField("Должность сотрудника", max_length=200)
    salary = models.DecimalField("Оклад", max_digits=10, decimal_places=2)
    age = models.IntegerField("Возраст")
    photo = models.ImageField("Фото", null=True, blank=True, upload_to=generate_file_path)
    department = models.ForeignKey(
        Department,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='employees'
    )

    @property
    def fio(self) -> str:
        return f"{self.name} {self.patronymic} {self.last_name}"

    def __str__(self):
        return f'{self.post}|{self.fio}'

    class Meta:
        db_table = 'employees'
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'
        indexes = [
            models.Index(fields=('last_name',), name='last_name_index')
        ]
