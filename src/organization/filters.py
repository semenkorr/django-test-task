import django_filters
from organization.models import Employee


class EmployeeModelFilter(django_filters.FilterSet):
    department_id = django_filters.NumberFilter(lookup_expr='exact')
    last_name = django_filters.CharFilter(lookup_expr='iexact')

    class Meta:
        model = Employee
        fields = ['department_id', 'last_name']
