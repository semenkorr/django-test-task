from rest_framework.serializers import ModelSerializer
from organization.models import Department, Employee
from rest_framework import serializers


class DepartmentModelSerializer(ModelSerializer):
    total_employees_salary = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)
    employees_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = Department
        fields = "__all__"


class EmployeeSerializer(ModelSerializer):
    class Meta:
        model = Employee
        fields = "__all__"
