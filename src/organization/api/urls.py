from rest_framework.routers import SimpleRouter
from organization.api import api


router = SimpleRouter()
router.register('departments', api.DepartmentModelViewSet, basename='departments')
router.register('employees', api.EmployeeModelViewSet, basename='employees')

urlpatterns = router.urls
