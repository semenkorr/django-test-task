from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from organization.api.serializers import DepartmentModelSerializer, EmployeeSerializer
from organization.filters import EmployeeModelFilter
from organization.models import Department, Employee
from rest_framework.pagination import LimitOffsetPagination


class DepartmentModelViewSet(ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentModelSerializer
    permission_classes = (AllowAny,)


class EmployeeModelViewSet(ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = LimitOffsetPagination
    filterset_class = EmployeeModelFilter
